package in.glivade.store2door;

import static in.glivade.store2door.app.Constant.EXTRA_AMOUNT;
import static in.glivade.store2door.app.Constant.EXTRA_ID;
import static in.glivade.store2door.app.MyActivity.launchWithBundle;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.glivade.store2door.adapter.CartAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.CartCallback;
import in.glivade.store2door.model.Cart;
import in.glivade.store2door.model.CartItem;
import in.glivade.store2door.model.CartItemQuantity;
import in.glivade.store2door.model.Data;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CartActivity extends AppCompatActivity implements CartCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable, View.OnClickListener {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private LinearLayout mLayoutMyCart;
    private RecyclerView mViewProducts;
    private TextView mTextViewTotal;
    private RelativeLayout mLayoutCheckout;
    private List<Cart> mCartList;
    private CartAdapter mCartAdapter;
    private MyPreference mPreference;
    private int mTotal;
    private boolean mShouldUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onBackPressed() {
        if (mShouldUpdate) {
            promptUpdateCartDialog();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onProductClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, mCartList.get(position).getProduct().getId());
        launchWithBundle(mContext, ProductDetailActivity.class, bundle);
    }

    @Override
    public void onMinusClick(int position) {
        mShouldUpdate = true;
        Cart cart = mCartList.get(position);
        int quantity = cart.getQuantity();
        if (quantity == 1) {
            deleteCartItem(mCartList.get(position).getId());
            mCartList.remove(position);
            mCartAdapter.notifyItemRemoved(position);
        } else {
            cart.setQuantity(quantity - 1);
            mCartAdapter.notifyItemChanged(position);
            updateCart();
        }
        updateCartTotal();
        isCartEmpty();
    }

    @Override
    public void onPlusClick(int position) {
        mShouldUpdate = true;
        Cart cart = mCartList.get(position);
        int quantity = cart.getQuantity();
        if (quantity < 9) {
            cart.setQuantity(quantity + 1);
            mCartAdapter.notifyItemChanged(position);
            updateCart();
            updateCartTotal();
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mLayoutCheckout) {
            if (mShouldUpdate) {
                promptUpdateCartDialog();
            } else {
                Bundle bundle = new Bundle();
                bundle.putInt(EXTRA_AMOUNT, mTotal);
                launchWithBundle(mContext, CheckoutActivity.class, bundle);
            }
        }
    }

    @Override
    public void onRefresh() {
        getCart();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getCart();
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mLayoutMyCart = (LinearLayout) findViewById(R.id.my_cart);
        mViewProducts = (RecyclerView) findViewById(R.id.products);
        mTextViewTotal = (TextView) findViewById(R.id.txt_total);
        mLayoutCheckout = (RelativeLayout) findViewById(R.id.checkout);

        mContext = this;
        mCartList = new ArrayList<>();
        mCartAdapter = new CartAdapter(mContext, mCartList, this);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mLayoutCheckout.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        mViewProducts.setLayoutManager(new LinearLayoutManager(mContext));
        mViewProducts.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mViewProducts.setAdapter(mCartAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void getCart() {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<List<Cart>> call = cartService.getCart("Token " + mPreference.getToken());
        call.enqueue(new Callback<List<Cart>>() {
            @Override
            public void onResponse(@NonNull Call<List<Cart>> call,
                    @NonNull Response<List<Cart>> response) {
                List<Cart> cartListResponse = response.body();
                if (response.isSuccessful() && cartListResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mLayoutMyCart.setVisibility(View.VISIBLE);
                    mCartList.clear();
                    mCartList.addAll(cartListResponse);
                    mCartAdapter.notifyDataSetChanged();
                    updateCartTotal();
                    isCartEmpty();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Cart>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void deleteCartItem(final int id) {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<Void> call = cartService.deleteCartItem("Token " + mPreference.getToken(), id);
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    mShouldUpdate = false;
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void updateCartTotal() {
        mTotal = 0;
        for (Cart cart : mCartList) {
            mTotal += cart.getQuantity() * cart.getProduct().getActualPrice();
        }
        mTextViewTotal.setText(
                String.format(Locale.getDefault(), getString(R.string.format_price), mTotal));
    }

    private void promptUpdateCartDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setMessage("You have unsaved changes. Do you want to update the cart?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateCart();
            }
        });
        builder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
                finish();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void updateCart() {
        List<CartItemQuantity> cartItemQuantityList = new ArrayList<>();
        for (Cart cart : mCartList) {
            cartItemQuantityList.add(new CartItemQuantity(cart.getId(), cart.getQuantity()));
        }
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<Data> call = cartService.updateCart("Token " + mPreference.getToken(),
                new CartItem(cartItemQuantityList));
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    mShouldUpdate = false;
                    ToastBuilder.build(mContext, dataResponse.getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void isCartEmpty() {
        if (mCartList.isEmpty()) {
            ToastBuilder.build(mContext, "Cart is empty");
            finish();
        }
    }
}
