package in.glivade.store2door;

import static in.glivade.store2door.app.Constant.EXTRA_ID;
import static in.glivade.store2door.app.MyActivity.launchWithBundle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import in.glivade.store2door.adapter.PromotionAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.ProductService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.PromotionCallback;
import in.glivade.store2door.model.Promotion;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OfferActivity extends AppCompatActivity implements PromotionCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mViewPromotions;
    private List<Promotion> mPromotionList;
    private PromotionAdapter mPromotionAdapter;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offer);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPromotionClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, mPromotionList.get(position).getSubCategory());
        launchWithBundle(mContext, ProductActivity.class, bundle);
    }

    @Override
    public void onRefresh() {
        getPromotions();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getPromotions();
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mViewPromotions = (RecyclerView) findViewById(R.id.promotions);

        mContext = this;
        mPromotionList = new ArrayList<>();
        mPromotionAdapter = new PromotionAdapter(mContext, mPromotionList, this);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        mViewPromotions.setLayoutManager(new LinearLayoutManager(mContext));
        mViewPromotions.setAdapter(mPromotionAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void getPromotions() {
        ProductService productService = ApiClient.getClient().create(ProductService.class);
        Call<List<Promotion>> call = productService.getPromotions(
                "Token " + mPreference.getToken());
        call.enqueue(new Callback<List<Promotion>>() {
            @Override
            public void onResponse(@NonNull Call<List<Promotion>> call,
                    @NonNull Response<List<Promotion>> response) {
                List<Promotion> promotionListResponse = response.body();
                if (response.isSuccessful() && promotionListResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mPromotionList.clear();
                    mPromotionList.addAll(promotionListResponse);
                    mPromotionAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Promotion>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }
}
