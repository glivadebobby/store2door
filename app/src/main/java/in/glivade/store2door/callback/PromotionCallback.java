package in.glivade.store2door.callback;

/**
 * Created by Bobby on 15/07/17
 */

public interface PromotionCallback {
    void onPromotionClick(int position);
}
