package in.glivade.store2door.callback;

/**
 * Created by Bobby on 02/08/17
 */

public interface CartCallback {
    void onProductClick(int position);

    void onMinusClick(int position);

    void onPlusClick(int position);
}
