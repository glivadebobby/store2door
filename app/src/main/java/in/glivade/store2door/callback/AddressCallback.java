package in.glivade.store2door.callback;

/**
 * Created by Bobby on 02/08/17
 */

public interface AddressCallback {
    void onAddressClick(int position);
}
