package in.glivade.store2door.callback;

/**
 * Created by Bobby on 16/07/17
 */

public interface SubCategoryCallback {
    void onSubCategoryClick(int position);
}
