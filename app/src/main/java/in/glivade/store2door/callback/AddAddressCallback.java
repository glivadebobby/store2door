package in.glivade.store2door.callback;

import in.glivade.store2door.model.Address;

/**
 * Created by Bobby on 02/08/17
 */

public interface AddAddressCallback {
    void onAddressAdded(Address address);
}
