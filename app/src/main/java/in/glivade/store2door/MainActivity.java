package in.glivade.store2door;

import static in.glivade.store2door.app.Constant.EXTRA_ID;
import static in.glivade.store2door.app.Constant.EXTRA_TITLE;
import static in.glivade.store2door.app.MyActivity.launch;
import static in.glivade.store2door.app.MyActivity.launchClearStack;
import static in.glivade.store2door.app.MyActivity.launchDialPad;
import static in.glivade.store2door.app.MyActivity.launchWithBundle;
import static in.glivade.store2door.app.MyActivity.shareApp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import in.glivade.store2door.adapter.CategoryAdapter;
import in.glivade.store2door.adapter.DailyOfferAdapter;
import in.glivade.store2door.adapter.PromotionAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.AuthService;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.api.ProductService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.CategoryCallback;
import in.glivade.store2door.callback.ProductCallback;
import in.glivade.store2door.callback.PromotionCallback;
import in.glivade.store2door.callback.SubCategoryCallback;
import in.glivade.store2door.model.Category;
import in.glivade.store2door.model.CreateCart;
import in.glivade.store2door.model.DailyOffer;
import in.glivade.store2door.model.Dashboard;
import in.glivade.store2door.model.NonFieldError;
import in.glivade.store2door.model.Product;
import in.glivade.store2door.model.Promotion;
import in.glivade.store2door.model.SubCategory;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener, ProductCallback, PromotionCallback,
        CategoryCallback, SwipeRefreshLayout.OnRefreshListener, Runnable, SubCategoryCallback {

    private static final int REQUEST_PLACE_PICKER = 4;
    private Context mContext;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout mRefreshLayout;
    private LinearLayout mLayoutDashboard;
    private RecyclerView mViewProducts, mViewPromotions, mViewCategories;
    private NavigationView mNavigationView;
    private ActionBarDrawerToggle mDrawerToggle;
    private List<DailyOffer> mDailyOfferList;
    private List<Promotion> mPromotionList;
    private List<Category> mCategoryList;
    private DailyOfferAdapter mDailyOfferAdapter;
    private PromotionAdapter mPromotionAdapter;
    private CategoryAdapter mCategoryAdapter;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initObjects();
        initCallbacks();
        initToolbar();
        initDrawer();
        initNavigationView();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPreference != null) getDashboard();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(Gravity.START);
                return true;
            case R.id.action_cart:
                launch(mContext, CartActivity.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawers();
        switch (item.getItemId()) {
            case R.id.nav_location:
                launchPlacePickerActivity();
                break;
            case R.id.nav_my_addresses:
                startActivity(new Intent(mContext, AddressActivity.class));
                break;
            case R.id.nav_my_orders:
                startActivity(new Intent(mContext, OrderActivity.class));
                break;
            case R.id.nav_my_wish_list:
                startActivity(new Intent(mContext, WishListActivity.class));
                break;
            case R.id.nav_my_cart:
                startActivity(new Intent(mContext, CartActivity.class));
                break;
            case R.id.nav_offer_zone:
                startActivity(new Intent(mContext, OfferActivity.class));
                break;
            case R.id.nav_need_help:
                launchDialPad(mContext, getString(R.string.nav_phone));
                break;
            case R.id.nav_rate_us:
                displayRateUsDialog();
                break;
            case R.id.nav_share:
                shareApp(mContext);
                break;
            case R.id.nav_about_us:
                startActivity(new Intent(this, AboutUsActivity.class));
                break;
            case R.id.nav_logout:
                promptLogoutDialog();
                break;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == REQUEST_PLACE_PICKER && data != null) {
            Place place = PlaceAutocomplete.getPlace(mContext, data);
            if (place != null) {
                Toast.makeText(this, place.getAddress(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onProductClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, mDailyOfferList.get(position).getProduct().getId());
        launchWithBundle(mContext, ProductDetailActivity.class, bundle);
    }

    @Override
    public void onAddClick(int position) {
        Product product = mDailyOfferList.get(position).getProduct();
        if (!product.isInCart()) addToCart(new CreateCart(product.getId(), 1));
    }

    @Override
    public void onPromotionClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, mPromotionList.get(position).getSubCategory());
        launchWithBundle(mContext, ProductActivity.class, bundle);
    }

    @Override
    public void onCategoryClick(int position) {
        for (int i = 0; i < mCategoryList.size(); i++) {
            if (i == position) continue;
            mCategoryList.get(i).setSelected(false);
        }
        Category category = mCategoryList.get(position);
        category.setSelected(!category.isSelected());
        mCategoryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSubCategoryClick(int position) {
        for (Category category : mCategoryList) {
            if (category.isSelected()) {
                SubCategory subCategory = category.getSubCategoryList().get(position);
                Bundle bundle = new Bundle();
                bundle.putInt(EXTRA_ID, subCategory.getId());
                bundle.putString(EXTRA_TITLE, subCategory.getName());
                launchWithBundle(mContext, ProductActivity.class, bundle);
                break;
            }
        }
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getDashboard();
    }

    @Override
    public void onRefresh() {
        getDashboard();
    }

    private void initObjects() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mProgressBar = (ProgressBar) findViewById(R.id.progress);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mLayoutDashboard = (LinearLayout) findViewById(R.id.dashboard);
        mViewProducts = (RecyclerView) findViewById(R.id.products);
        mViewPromotions = (RecyclerView) findViewById(R.id.promotions);
        mViewCategories = (RecyclerView) findViewById(R.id.categories);
        mNavigationView = (NavigationView) findViewById(R.id.nav);

        mContext = this;
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close);
        mDailyOfferList = new ArrayList<>();
        mPromotionList = new ArrayList<>();
        mCategoryList = new ArrayList<>();
        mDailyOfferAdapter = new DailyOfferAdapter(mContext, mDailyOfferList, this);
        mPromotionAdapter = new PromotionAdapter(mContext, mPromotionList, this);
        mCategoryAdapter = new CategoryAdapter(mContext, mCategoryList, this, this);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mNavigationView.setNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
    }

    private void initDrawer() {
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    private void initNavigationView() {
        MenuItem menuItem = mNavigationView.getMenu().findItem(R.id.nav_phone);
        menuItem.setTitle(mPreference.getPhone());
    }

    private void initRecyclerView() {
        mViewProducts.setLayoutManager(
                new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mViewProducts.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.HORIZONTAL));
        mViewProducts.setAdapter(mDailyOfferAdapter);

        mViewPromotions.setLayoutManager(
                new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
        mViewPromotions.setAdapter(mPromotionAdapter);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(mViewPromotions);

        mViewCategories.setLayoutManager(new LinearLayoutManager(mContext));
        mViewCategories.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mViewCategories.setAdapter(mCategoryAdapter);
        mViewCategories.setNestedScrollingEnabled(false);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void getDashboard() {
        ProductService productService = ApiClient.getClient().create(ProductService.class);
        Call<Dashboard> call = productService.getDashboard("Token " + mPreference.getToken());
        call.enqueue(new Callback<Dashboard>() {
            @Override
            public void onResponse(@NonNull Call<Dashboard> call,
                    @NonNull Response<Dashboard> response) {
                Dashboard dashboardResponse = response.body();
                if (response.isSuccessful() && dashboardResponse != null) {
                    displayProgress(false);
                    mRefreshLayout.setRefreshing(false);
                    mDailyOfferList.clear();
                    mPromotionList.clear();
                    mCategoryList.clear();
                    mDailyOfferList.addAll(dashboardResponse.getDailyOfferList());
                    mPromotionList.addAll(dashboardResponse.getPromotionList());
                    mCategoryList.addAll(dashboardResponse.getCategoryList());
                    mDailyOfferAdapter.notifyDataSetChanged();
                    mPromotionAdapter.notifyDataSetChanged();
                    mCategoryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Dashboard> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void addToCart(CreateCart createCart) {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<CreateCart> call = cartService.createCart("Token " + mPreference.getToken(),
                createCart);
        call.enqueue(new Callback<CreateCart>() {
            @Override
            public void onResponse(@NonNull Call<CreateCart> call,
                    @NonNull Response<CreateCart> response) {
                CreateCart createCartResponse = response.body();
                if (response.isSuccessful() && createCartResponse != null) {
                    for (int i = 0; i < mDailyOfferList.size(); i++) {
                        Product product = mDailyOfferList.get(i).getProduct();
                        if (createCartResponse.getProduct() == product.getId()) {
                            product.setInCart(true);
                            mDailyOfferAdapter.notifyItemChanged(i);
                            break;
                        }
                    }
                } else {
                    processError(response);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CreateCart> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void processError(Response<CreateCart> response) {
        int statusCode = response.code();
        if (statusCode == 400 && response.errorBody() != null) {
            try {
                Converter<ResponseBody, NonFieldError> converter =
                        ApiClient.getClient().responseBodyConverter(NonFieldError.class,
                                new Annotation[0]);
                //noinspection ConstantConditions
                NonFieldError nonFieldError = converter.convert(response.errorBody());
                if (nonFieldError != null) {
                    ToastBuilder.build(mContext, nonFieldError.getMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ToastBuilder.build(mContext, getString(R.string.error_unexpected));
        }
    }

    private void launchPlacePickerActivity() {
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), REQUEST_PLACE_PICKER);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException
                e) {
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void displayRateUsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(R.layout.dialog_rating);

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void promptLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to Logout?");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                displayProgress(true);
                logout();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void logout() {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<Void> call = authService.logout("Token " + mPreference.getToken());
        call.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NonNull Call<Void> call, @NonNull Response<Void> response) {
                if (response.isSuccessful()) {
                    mPreference.clearUser();
                    launchClearStack(mContext, SplashActivity.class);
                    ToastBuilder.build(mContext, getString(R.string.alert_logged_out));
                }
            }

            @Override
            public void onFailure(@NonNull Call<Void> call, @NonNull Throwable t) {
                displayProgress(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void displayProgress(boolean display) {
        if (display) {
            mProgressBar.setVisibility(View.VISIBLE);
            mLayoutDashboard.setVisibility(View.GONE);
        } else {
            mProgressBar.setVisibility(View.GONE);
            mLayoutDashboard.setVisibility(View.VISIBLE);
        }
    }
}
