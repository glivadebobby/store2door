package in.glivade.store2door;

import static in.glivade.store2door.app.MyActivity.launchClearStack;
import static in.glivade.store2door.helper.Utils.getMacAddress;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

import java.io.IOException;
import java.lang.annotation.Annotation;

import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.AuthService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.model.AuthError;
import in.glivade.store2door.model.Login;
import in.glivade.store2door.model.User;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_READ_SMS = 4;
    private static final int REQUEST_GET_ACCOUNTS = 27;
    private static final int REQUEST_LOGIN_EMAIL = 4;
    private static final int REQUEST_LOGIN_PHONE = 27;
    private Context mContext;
    private TextInputEditText mEditTextName, mEditTextPhone, mEditTextEmail;
    private Button mButtonRegister, mButtonLoginEmail, mButtonLoginPhone;
    private LinearLayout mLayoutProgress, mLayoutFooter;
    private MyPreference mPreference;
    private boolean mRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        initObjects();
        initCallbacks();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
            @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_SMS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mRegister) {
                        processRegister();
                    } else {
                        loginPhone();
                    }
                }
                break;
            case REQUEST_GET_ACCOUNTS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    loginEmail();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOGIN_PHONE) {
            AccountKitLoginResult loginResult = data.getParcelableExtra(
                    AccountKitLoginResult.RESULT_KEY);
            if (loginResult.getError() != null) {
                ToastBuilder.build(mContext, loginResult.getError().getErrorType().getMessage());
            } else if (loginResult.wasCancelled()) {
                ToastBuilder.build(mContext, "Login Cancelled");
            } else if (loginResult.getAccessToken() != null) {
                displayProgress(true);
                processLogin();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonRegister) {
            processRegister();
        } else if (view == mButtonLoginEmail) {
            if (hasGetAccountsPermission()) {
                loginEmail();
            } else {
                requestGetAccountsPermission();
            }
        } else if (view == mButtonLoginPhone) {
            if (hasReadSmsPermission()) {
                loginPhone();
            } else {
                mRegister = false;
                requestReadSmsPermission();
            }
        }
    }

    private void initObjects() {
        mEditTextName = (TextInputEditText) findViewById(R.id.input_name);
        mEditTextPhone = (TextInputEditText) findViewById(R.id.input_phone);
        mEditTextEmail = (TextInputEditText) findViewById(R.id.input_email);
        mButtonRegister = (Button) findViewById(R.id.btn_register);
        mButtonLoginEmail = (Button) findViewById(R.id.btn_login_email);
        mButtonLoginPhone = (Button) findViewById(R.id.btn_login_phone);
        mLayoutProgress = (LinearLayout) findViewById(R.id.progress);
        mLayoutFooter = (LinearLayout) findViewById(R.id.footer);

        mContext = this;
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mButtonRegister.setOnClickListener(this);
        mButtonLoginEmail.setOnClickListener(this);
        mButtonLoginPhone.setOnClickListener(this);
    }

    private void processRegister() {
        String name = mEditTextName.getText().toString().trim();
        String phone = mEditTextPhone.getText().toString().trim();
        String email = mEditTextEmail.getText().toString().trim();
        if (validateInput(name, phone, email)) {
            if (hasReadSmsPermission()) {
                displayProgress(true);
                registerUser(new User(name, phone, email));
            } else {
                mRegister = true;
                requestReadSmsPermission();
            }
        }
    }

    private boolean validateInput(String name, String phone, String email) {
        if (name.isEmpty()) {
            mEditTextName.requestFocus();
            mEditTextName.setError(getString(R.string.error_empty));
            return false;
        } else if (phone.isEmpty()) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(getString(R.string.error_empty));
            return false;
        } else if (phone.length() < 10) {
            mEditTextPhone.requestFocus();
            mEditTextPhone.setError(getString(R.string.error_phone_length));
            return false;
        } else if (email.isEmpty()) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(getString(R.string.error_empty));
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEditTextEmail.requestFocus();
            mEditTextEmail.setError(getString(R.string.error_email_invalid));
            return false;
        }
        return true;
    }

    private void registerUser(User user) {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<User> call = authService.registerUser(user);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                displayProgress(false);
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    mPreference.setPhone(userResponse.getUsername());
                    loginPhone();
                } else {
                    processError(response);
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                displayProgress(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void processError(Response<User> response) {
        int statusCode = response.code();
        if (statusCode == 400 && response.errorBody() != null) {
            try {
                Converter<ResponseBody, AuthError> converter =
                        ApiClient.getClient().responseBodyConverter(AuthError.class,
                                new Annotation[0]);
                //noinspection ConstantConditions
                AuthError authError = converter.convert(response.errorBody());
                if (authError.getUsername() != null) {
                    mEditTextPhone.setError(authError.getUsername().getMessage());
                }
                if (authError.getEmail() != null) {
                    mEditTextEmail.setError(authError.getEmail().getMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ToastBuilder.build(mContext, getString(R.string.error_unexpected));
        }
    }

    private void loginUser(Login login) {
        AuthService authService = ApiClient.getClient().create(AuthService.class);
        Call<User> call = authService.loginUser(login);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                User userResponse = response.body();
                if (response.isSuccessful() && userResponse != null) {
                    ToastBuilder.build(mContext, "Welcome, " + userResponse.getName());
                    mPreference.setName(userResponse.getName());
                    mPreference.setPhone(userResponse.getUsername());
                    mPreference.setEmail(userResponse.getEmail());
                    mPreference.setToken(userResponse.getToken());
                    launchClearStack(mContext, MainActivity.class);
                }
            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                displayProgress(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void loginEmail() {
        Intent intent = new Intent(mContext, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.EMAIL,
                        AccountKitActivity.ResponseType.TOKEN);
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, REQUEST_LOGIN_EMAIL);
    }

    private void loginPhone() {
        Intent intent = new Intent(mContext, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        String phone = mPreference.getPhone();
        if (phone != null) {
            configurationBuilder.setInitialPhoneNumber(new PhoneNumber("91", phone, "IN"));
        }
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, REQUEST_LOGIN_PHONE);
    }

    private void processLogin() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(Account account) {
                String phoneNumber = account.getPhoneNumber().getPhoneNumber();
                mPreference.setPhone(phoneNumber);
                loginUser(new Login(mPreference.getPhone(), getMacAddress()));
            }

            @Override
            public void onError(AccountKitError error) {
                displayProgress(false);
                ToastBuilder.build(mContext, error.getUserFacingMessage());
            }
        });
    }

    private boolean hasReadSmsPermission() {
        return ContextCompat.checkSelfPermission(mContext, android.Manifest.permission.READ_SMS)
                == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasGetAccountsPermission() {
        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.GET_ACCOUNTS)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestReadSmsPermission() {
        ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_SMS},
                REQUEST_READ_SMS);
    }

    private void requestGetAccountsPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{android.Manifest.permission.GET_ACCOUNTS}, REQUEST_GET_ACCOUNTS);
    }

    private void displayProgress(boolean show) {
        if (show) {
            mLayoutProgress.setVisibility(View.VISIBLE);
            mLayoutFooter.setVisibility(View.GONE);
        } else {
            mLayoutProgress.setVisibility(View.GONE);
            mLayoutFooter.setVisibility(View.VISIBLE);
        }
    }
}
