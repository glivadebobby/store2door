package in.glivade.store2door;

import static in.glivade.store2door.app.Constant.EXTRA_ID;
import static in.glivade.store2door.app.MyActivity.launch;

import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Locale;

import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.api.ProductService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.model.CreateCart;
import in.glivade.store2door.model.NonFieldError;
import in.glivade.store2door.model.Product;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductDetailActivity extends AppCompatActivity implements View.OnClickListener,
        Runnable, SwipeRefreshLayout.OnRefreshListener {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private LinearLayout mLayoutProductDetail;
    private ImageView mImageViewProduct;
    private TextView mTextViewDiscount, mTextViewName, mTextViewApparentPrice, mTextViewActualPrice,
            mTextViewAdd, mTextViewUnit, mTextViewKeyFeatures, mTextViewContents;
    private MyPreference mPreference;
    private int mId, mProductId;
    private boolean mInCart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        initObjects();
        initCallbacks();
        processBundle();
        initToolbar();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPreference != null) getProductDetail();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_cart:
                launch(mContext, CartActivity.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mTextViewAdd) {
            if (!mInCart) addToCart(new CreateCart(mProductId, 1));
        }
    }

    @Override
    public void onRefresh() {
        getProductDetail();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getProductDetail();
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mLayoutProductDetail = (LinearLayout) findViewById(R.id.product_detail);
        mImageViewProduct = (ImageView) findViewById(R.id.img_product);
        mTextViewDiscount = (TextView) findViewById(R.id.txt_discount);
        mTextViewName = (TextView) findViewById(R.id.txt_name);
        mTextViewApparentPrice = (TextView) findViewById(R.id.txt_apparent_price);
        mTextViewActualPrice = (TextView) findViewById(R.id.txt_actual_price);
        mTextViewAdd = (TextView) findViewById(R.id.txt_add);
        mTextViewUnit = (TextView) findViewById(R.id.txt_unit);
        mTextViewKeyFeatures = (TextView) findViewById(R.id.txt_key_features);
        mTextViewContents = (TextView) findViewById(R.id.txt_contents);

        mContext = this;
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mTextViewAdd.setOnClickListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mId = bundle.getInt(EXTRA_ID);
        }
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void getProductDetail() {
        ProductService productService = ApiClient.getClient().create(ProductService.class);
        Call<Product> call = productService.getProductDetail("Token " + mPreference.getToken(),
                mId);
        call.enqueue(new Callback<Product>() {
            @Override
            public void onResponse(@NonNull Call<Product> call,
                    @NonNull Response<Product> response) {
                Product productResponse = response.body();
                if (response.isSuccessful() && productResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mLayoutProductDetail.setVisibility(View.VISIBLE);
                    mProductId = productResponse.getId();
                    Glide.with(mContext).load(productResponse.getImage()).into(mImageViewProduct);
                    mTextViewApparentPrice.setPaintFlags(
                            mTextViewApparentPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    if (productResponse.getDiscount().equals("0.0")) {
                        mTextViewDiscount.setVisibility(View.GONE);
                    } else {
                        mTextViewDiscount.setVisibility(View.VISIBLE);
                        mTextViewDiscount.setText(productResponse.getDiscount());
                    }
                    mTextViewName.setText(productResponse.getName());
                    mTextViewApparentPrice.setText(
                            String.format(Locale.getDefault(), getString(R.string.format_price),
                                    productResponse.getApparentPrice()));
                    mTextViewActualPrice.setText(
                            String.format(Locale.getDefault(), getString(R.string.format_price),
                                    productResponse.getActualPrice()));
                    mTextViewUnit.setText(productResponse.getUnit());
                    mTextViewKeyFeatures.setText(productResponse.getDescription());
                    mTextViewContents.setText(productResponse.getContents());
                    mInCart = productResponse.isInCart();
                    invalidateAddButton();
                }
            }

            @Override
            public void onFailure(@NonNull Call<Product> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void addToCart(CreateCart createCart) {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<CreateCart> call = cartService.createCart("Token " + mPreference.getToken(),
                createCart);
        call.enqueue(new Callback<CreateCart>() {
            @Override
            public void onResponse(@NonNull Call<CreateCart> call,
                    @NonNull Response<CreateCart> response) {
                if (response.isSuccessful()) {
                    mInCart = true;
                    invalidateAddButton();
                    ToastBuilder.build(mContext, getString(R.string.alert_added_cart));
                } else {
                    processError(response);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CreateCart> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void processError(Response<CreateCart> response) {
        int statusCode = response.code();
        if (statusCode == 400 && response.errorBody() != null) {
            try {
                Converter<ResponseBody, NonFieldError> converter =
                        ApiClient.getClient().responseBodyConverter(NonFieldError.class,
                                new Annotation[0]);
                //noinspection ConstantConditions
                NonFieldError nonFieldError = converter.convert(response.errorBody());
                if (nonFieldError != null) {
                    ToastBuilder.build(mContext, nonFieldError.getMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ToastBuilder.build(mContext, getString(R.string.error_unexpected));
        }
    }

    private void invalidateAddButton() {
        if (mInCart) {
            mTextViewAdd.setBackground(null);
            mTextViewAdd.setText(getString(R.string.txt_added_cart));
        } else {
            mTextViewAdd.setBackgroundResource(R.drawable.bg_primary_stroke_round);
            mTextViewAdd.setText(getString(R.string.txt_add));
        }
    }
}
