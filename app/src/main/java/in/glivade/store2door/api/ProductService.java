package in.glivade.store2door.api;

import java.util.List;

import in.glivade.store2door.model.Dashboard;
import in.glivade.store2door.model.Product;
import in.glivade.store2door.model.ProductWishList;
import in.glivade.store2door.model.Promotion;
import in.glivade.store2door.model.WishList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Bobby on 01/08/17
 */

public interface ProductService {

    @GET("users/home-page/")
    Call<Dashboard> getDashboard(@Header("Authorization") String token);

    @GET("users/list/products/{id}/")
    Call<List<Product>> getProducts(@Header("Authorization") String token,
            @Path("id") int subCategoryId);

    @GET("users/retrieve/product/{id}/")
    Call<Product> getProductDetail(@Header("Authorization") String token,
            @Path("id") int productId);

    @POST("users/create/destroy/wishlist/{id}/")
    Call<WishList> updateWishList(@Header("Authorization") String token, @Path("id") int productId);

    @GET("users/list/my-wishlist/")
    Call<List<ProductWishList>> getMyWishList(@Header("Authorization") String token);

    @GET("users/list/promotions/")
    Call<List<Promotion>> getPromotions(@Header("Authorization") String token);
}
