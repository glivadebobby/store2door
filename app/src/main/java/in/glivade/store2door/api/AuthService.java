package in.glivade.store2door.api;

import in.glivade.store2door.model.Login;
import in.glivade.store2door.model.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Bobby on 30/07/17
 */

public interface AuthService {

    @POST("auth/register/")
    Call<User> registerUser(@Body User user);

    @POST("auth/validate-user/")
    Call<User> loginUser(@Body Login login);

    @POST("auth/logout/")
    Call<Void> logout(@Header("Authorization") String token);
}
