package in.glivade.store2door.api;

import java.util.List;

import in.glivade.store2door.model.Address;
import in.glivade.store2door.model.Cart;
import in.glivade.store2door.model.CartItem;
import in.glivade.store2door.model.Checkout;
import in.glivade.store2door.model.CreateCart;
import in.glivade.store2door.model.Data;
import in.glivade.store2door.model.Order;
import in.glivade.store2door.model.PaymentSuccess;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by Bobby on 02/08/17
 */

public interface CartService {

    @POST("users/list/create/my-cart/")
    Call<CreateCart> createCart(@Header("Authorization") String token, @Body CreateCart createCart);

    @GET("users/list/create/my-cart/")
    Call<List<Cart>> getCart(@Header("Authorization") String token);

    @DELETE("users/destroy/my-cart/{id}/")
    Call<Void> deleteCartItem(@Header("Authorization") String token, @Path("id") int mCartId);

    @PUT("users/update/cart/")
    Call<Data> updateCart(@Header("Authorization") String token, @Body CartItem cartItem);

    @GET("users/my-cart/checkout/")
    Call<Checkout> checkoutCart(@Header("Authorization") String token);

    @GET("users/list/create/delivery-address/")
    Call<List<Address>> getAddresses(@Header("Authorization") String token);

    @POST("users/list/create/delivery-address/")
    Call<Address> addAddress(@Header("Authorization") String token, @Body Address address);

    @POST("users/payment-success/")
    Call<Data> paymentSuccess(@Header("Authorization") String token,
            @Body PaymentSuccess paymentSuccess);

    @GET("users/my-orders/")
    Call<List<Order>> getOrders(@Header("Authorization") String token);
}
