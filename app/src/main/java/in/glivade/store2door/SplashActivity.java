package in.glivade.store2door;

import static in.glivade.store2door.app.MyActivity.launch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import in.glivade.store2door.app.MyPreference;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyPreference preference = new MyPreference(this);
        if (preference.getToken() == null) {
            launch(this, WelcomeActivity.class);
        } else {
            launch(this, MainActivity.class);
        }
    }
}
