package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 01/08/17
 */

public class SubCategory {

    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("image")
    private String mImage;

    public SubCategory(int id, String name, String image) {
        mId = id;
        mName = name;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }
}
