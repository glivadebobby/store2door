package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class Data {

    @SerializedName("data")
    private String mData;

    public Data(String data) {
        mData = data;
    }

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }
}
