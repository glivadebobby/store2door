package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class ProductWishList {

    @SerializedName("id")
    private int mId;
    @SerializedName("user")
    private int mUser;
    @SerializedName("product")
    private Product mProduct;

    public ProductWishList(int id, int user, Product product) {
        mId = id;
        mUser = user;
        mProduct = product;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getUser() {
        return mUser;
    }

    public void setUser(int user) {
        mUser = user;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }
}
