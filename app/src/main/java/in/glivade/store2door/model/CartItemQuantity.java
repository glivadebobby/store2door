package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class CartItemQuantity {

    @SerializedName("pk")
    private int mPk;
    @SerializedName("quantity")
    private int mQuantity;

    public CartItemQuantity(int pk, int quantity) {
        mPk = pk;
        mQuantity = quantity;
    }

    public int getPk() {
        return mPk;
    }

    public void setPk(int pk) {
        mPk = pk;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }
}
