package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class WishList {

    @SerializedName("in_wishlist")
    private boolean mWishList;

    public WishList(boolean wishList) {
        mWishList = wishList;
    }

    public boolean isWishList() {
        return mWishList;
    }

    public void setWishList(boolean wishList) {
        mWishList = wishList;
    }
}
