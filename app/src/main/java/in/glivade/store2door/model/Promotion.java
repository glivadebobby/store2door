package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 15/07/17
 */

public class Promotion {

    @SerializedName("id")
    private int mId;
    @SerializedName("sub_category")
    private int mSubCategory;
    @SerializedName("image")
    private String mImage;

    public Promotion(int id, int subCategory, String image) {
        mId = id;
        mSubCategory = subCategory;
        mImage = image;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getSubCategory() {
        return mSubCategory;
    }

    public void setSubCategory(int subCategory) {
        mSubCategory = subCategory;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }
}
