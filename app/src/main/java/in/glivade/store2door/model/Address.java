package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class Address {

    @SerializedName("id")
    private int mId;
    @SerializedName("street")
    private String mStreet;
    @SerializedName("area")
    private String mArea;
    @SerializedName("city")
    private String mCity;
    @SerializedName("state")
    private String mState;
    @SerializedName("country")
    private String mCountry;
    @SerializedName("zipcode")
    private String mZipCode;
    private boolean mSelected;

    public Address(String street, String area, String city, String state, String country,
            String zipCode) {
        mStreet = street;
        mArea = area;
        mCity = city;
        mState = state;
        mCountry = country;
        mZipCode = zipCode;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getArea() {
        return mArea;
    }

    public void setArea(String area) {
        mArea = area;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getZipCode() {
        return mZipCode;
    }

    public void setZipCode(String zipCode) {
        mZipCode = zipCode;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }

    public String getAddress() {
        return mStreet + "\n" + mArea + "\n" + mCity + " " + mState + "\n" + mCountry + " "
                + mZipCode;
    }
}
