package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 04/08/17
 */

public class Store {

    @SerializedName("id")
    private int mId;
    @SerializedName("free_deliver_cost")
    private int mFreeDeliveryCost;
    @SerializedName("rating")
    private float mRating;
    @SerializedName("name")
    private String mName;
    @SerializedName("delivery_time")
    private String mDeliveryTime;

    public Store(int id, int freeDeliveryCost, float rating, String name, String deliveryTime) {
        mId = id;
        mFreeDeliveryCost = freeDeliveryCost;
        mRating = rating;
        mName = name;
        mDeliveryTime = deliveryTime;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getFreeDeliveryCost() {
        return mFreeDeliveryCost;
    }

    public void setFreeDeliveryCost(int freeDeliveryCost) {
        mFreeDeliveryCost = freeDeliveryCost;
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float rating) {
        mRating = rating;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDeliveryTime() {
        return mDeliveryTime;
    }

    public void setDeliveryTime(String deliveryTime) {
        mDeliveryTime = deliveryTime;
    }
}
