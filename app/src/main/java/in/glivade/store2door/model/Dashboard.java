package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bobby on 01/08/17
 */

public class Dashboard {

    @SerializedName("daily_offers")
    private List<DailyOffer> mDailyOfferList;
    @SerializedName("promotion")
    private List<Promotion> mPromotionList;
    @SerializedName("product_category")
    private List<Category> mCategoryList;

    public Dashboard(List<DailyOffer> dailyOfferList, List<Promotion> promotionList,
            List<Category> categoryList) {
        mDailyOfferList = dailyOfferList;
        mPromotionList = promotionList;
        mCategoryList = categoryList;
    }

    public List<DailyOffer> getDailyOfferList() {
        return mDailyOfferList;
    }

    public void setDailyOfferList(List<DailyOffer> dailyOfferList) {
        mDailyOfferList = dailyOfferList;
    }

    public List<Promotion> getPromotionList() {
        return mPromotionList;
    }

    public void setPromotionList(List<Promotion> promotionList) {
        mPromotionList = promotionList;
    }

    public List<Category> getCategoryList() {
        return mCategoryList;
    }

    public void setCategoryList(List<Category> categoryList) {
        mCategoryList = categoryList;
    }
}
