package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 04/08/17
 */

public class Login {

    @SerializedName("username")
    private String mUsername;
    @SerializedName("client")
    private String mClient;

    public Login(String username, String client) {
        mUsername = username;
        mClient = client;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

    public String getClient() {
        return mClient;
    }

    public void setClient(String client) {
        mClient = client;
    }
}
