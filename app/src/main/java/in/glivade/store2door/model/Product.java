package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 16/07/17
 */

public class Product {

    @SerializedName("id")
    private int mId;
    @SerializedName("apparent_price")
    private int mApparentPrice;
    @SerializedName("actual_price")
    private int mActualPrice;
    @SerializedName("quantity")
    private int mQuantity;
    @SerializedName("name")
    private String mName;
    @SerializedName("unit")
    private String mUnit;
    @SerializedName("image")
    private String mImage;
    @SerializedName("discount")
    private String mDiscount;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("contents")
    private String mContents;
    @SerializedName("in_wishlist")
    private boolean mInWishList;
    @SerializedName("in_cart")
    private boolean mInCart;

    public Product(int id, int apparentPrice, int actualPrice, int quantity, String name,
            String unit, String image, String discount) {
        mId = id;
        mApparentPrice = apparentPrice;
        mActualPrice = actualPrice;
        mQuantity = quantity;
        mName = name;
        mUnit = unit;
        mImage = image;
        mDiscount = discount;
    }

    public Product(int id, int apparentPrice, int actualPrice, int quantity, String name,
            String unit, String image, String discount, String description, String contents,
            boolean inWishList, boolean inCart) {
        mId = id;
        mApparentPrice = apparentPrice;
        mActualPrice = actualPrice;
        mQuantity = quantity;
        mName = name;
        mUnit = unit;
        mImage = image;
        mDiscount = discount;
        mDescription = description;
        mContents = contents;
        mInWishList = inWishList;
        mInCart = inCart;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getApparentPrice() {
        return mApparentPrice;
    }

    public void setApparentPrice(int apparentPrice) {
        mApparentPrice = apparentPrice;
    }

    public int getActualPrice() {
        return mActualPrice;
    }

    public void setActualPrice(int actualPrice) {
        mActualPrice = actualPrice;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUnit() {
        return mUnit;
    }

    public void setUnit(String unit) {
        mUnit = unit;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getDiscount() {
        return mDiscount;
    }

    public void setDiscount(String discount) {
        mDiscount = discount;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getContents() {
        return mContents;
    }

    public void setContents(String contents) {
        mContents = contents;
    }

    public boolean isInWishList() {
        return mInWishList;
    }

    public void setInWishList(boolean inWishList) {
        mInWishList = inWishList;
    }

    public boolean isInCart() {
        return mInCart;
    }

    public void setInCart(boolean inCart) {
        mInCart = inCart;
    }
}
