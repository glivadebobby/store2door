package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bobby on 16/07/17
 */

public class Category {

    @SerializedName("id")
    private int mId;
    @SerializedName("name")
    private String mName;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("image")
    private String mImage;
    @SerializedName("sub_category")
    private List<SubCategory> mSubCategoryList;
    private boolean mSelected;

    public Category(int id, String name, String description, String image,
            List<SubCategory> subCategoryList, boolean selected) {
        mId = id;
        mName = name;
        mDescription = description;
        mImage = image;
        mSubCategoryList = subCategoryList;
        mSelected = selected;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public List<SubCategory> getSubCategoryList() {
        return mSubCategoryList;
    }

    public void setSubCategoryList(List<SubCategory> subCategoryList) {
        mSubCategoryList = subCategoryList;
    }

    public boolean isSelected() {
        return mSelected;
    }

    public void setSelected(boolean selected) {
        mSelected = selected;
    }
}
