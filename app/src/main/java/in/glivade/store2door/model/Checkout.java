package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class Checkout {

    @SerializedName("amount")
    private int mAmount;
    @SerializedName("order_id")
    private String mOrderId;

    public Checkout(int amount, String orderId) {
        mAmount = amount;
        mOrderId = orderId;
    }

    public int getAmount() {
        return mAmount;
    }

    public void setAmount(int amount) {
        mAmount = amount;
    }

    public String getOrderId() {
        return mOrderId;
    }

    public void setOrderId(String orderId) {
        mOrderId = orderId;
    }
}
