package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bobby on 02/08/17
 */

public class CartItem {

    @SerializedName("cart_items")
    private List<CartItemQuantity> mCartItemQuantityList;

    public CartItem(List<CartItemQuantity> cartItemQuantityList) {
        mCartItemQuantityList = cartItemQuantityList;
    }

    public List<CartItemQuantity> getCartItemQuantityList() {
        return mCartItemQuantityList;
    }

    public void setCartItemQuantityList(
            List<CartItemQuantity> cartItemQuantityList) {
        mCartItemQuantityList = cartItemQuantityList;
    }
}
