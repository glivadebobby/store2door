package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 04/08/17
 */

public class OrderedProduct {

    @SerializedName("id")
    private int mId;
    @SerializedName("order")
    private int mOrder;
    @SerializedName("quantity")
    private int mQuantity;
    @SerializedName("product")
    private Product mProduct;

    public OrderedProduct(int id, int order, int quantity, Product product) {
        mId = id;
        mOrder = order;
        mQuantity = quantity;
        mProduct = product;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getOrder() {
        return mOrder;
    }

    public void setOrder(int order) {
        mOrder = order;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }
}
