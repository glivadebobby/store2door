package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class Cart {

    @SerializedName("id")
    private int mId;
    @SerializedName("quantity")
    private int mQuantity;
    @SerializedName("product")
    private Product mProduct;

    public Cart(int id, int quantity, Product product) {
        mId = id;
        mQuantity = quantity;
        mProduct = product;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }
}
