package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 04/08/17
 */

public class AuthError {

    @SerializedName("username")
    private Username mUsername;
    @SerializedName("email")
    private Email mEmail;

    public AuthError(Username username, Email email) {
        mUsername = username;
        mEmail = email;
    }

    public Username getUsername() {
        return mUsername;
    }

    public void setUsername(Username username) {
        mUsername = username;
    }

    public Email getEmail() {
        return mEmail;
    }

    public void setEmail(Email email) {
        mEmail = email;
    }
}
