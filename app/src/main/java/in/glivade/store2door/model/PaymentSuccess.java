package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 03/08/17
 */

public class PaymentSuccess {

    @SerializedName("store")
    private int mStore;
    @SerializedName("address")
    private int mAddress;
    @SerializedName("razorpay_order_id")
    private String mRazorpayOrderId;
    @SerializedName("razorpay_payment_id")
    private String mRazorpayPaymentId;
    @SerializedName("razorpay_signature")
    private String mRazorpaySignature;

    public PaymentSuccess(int store, int address, String razorpayOrderId,
            String razorpayPaymentId, String razorpaySignature) {
        mStore = store;
        mAddress = address;
        mRazorpayOrderId = razorpayOrderId;
        mRazorpayPaymentId = razorpayPaymentId;
        mRazorpaySignature = razorpaySignature;
    }

    public int getStore() {
        return mStore;
    }

    public void setStore(int store) {
        mStore = store;
    }

    public int getAddress() {
        return mAddress;
    }

    public void setAddress(int address) {
        mAddress = address;
    }

    public String getRazorpayOrderId() {
        return mRazorpayOrderId;
    }

    public void setRazorpayOrderId(String razorpayOrderId) {
        mRazorpayOrderId = razorpayOrderId;
    }

    public String getRazorpayPaymentId() {
        return mRazorpayPaymentId;
    }

    public void setRazorpayPaymentId(String razorpayPaymentId) {
        mRazorpayPaymentId = razorpayPaymentId;
    }

    public String getRazorpaySignature() {
        return mRazorpaySignature;
    }

    public void setRazorpaySignature(String razorpaySignature) {
        mRazorpaySignature = razorpaySignature;
    }
}
