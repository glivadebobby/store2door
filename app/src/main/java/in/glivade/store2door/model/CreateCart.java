package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 02/08/17
 */

public class CreateCart {

    @SerializedName("id")
    private int mId;
    @SerializedName("product")
    private int mProduct;
    @SerializedName("quantity")
    private int mQuantity;

    public CreateCart(int product, int quantity) {
        mProduct = product;
        mQuantity = quantity;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getProduct() {
        return mProduct;
    }

    public void setProduct(int product) {
        mProduct = product;
    }

    public int getQuantity() {
        return mQuantity;
    }

    public void setQuantity(int quantity) {
        mQuantity = quantity;
    }
}
