package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Bobby on 04/08/17
 */

public class Order {

    @SerializedName("id")
    private int mId;
    @SerializedName("total_cost")
    private int mTotalCost;
    @SerializedName("created_on")
    private String mCreatedOn;
    @SerializedName("store")
    private Store mStore;
    @SerializedName("address")
    private Address mAddress;
    @SerializedName("ordered_items")
    private List<OrderedProduct> mOrderedProductList;

    public Order(int id, int totalCost, String createdOn, Store store, Address address,
            List<OrderedProduct> orderedProductList) {
        mId = id;
        mTotalCost = totalCost;
        mCreatedOn = createdOn;
        mStore = store;
        mAddress = address;
        mOrderedProductList = orderedProductList;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getTotalCost() {
        return mTotalCost;
    }

    public void setTotalCost(int totalCost) {
        mTotalCost = totalCost;
    }

    public String getCreatedOn() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS",
                Locale.getDefault());
        try {
            Date date = dateFormat.parse(mCreatedOn);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM hh:mm a",
                    Locale.getDefault());
            return simpleDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mCreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        mCreatedOn = createdOn;
    }

    public Store getStore() {
        return mStore;
    }

    public void setStore(Store store) {
        mStore = store;
    }

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address address) {
        mAddress = address;
    }

    public List<OrderedProduct> getOrderedProductList() {
        return mOrderedProductList;
    }

    public void setOrderedProductList(List<OrderedProduct> orderedProductList) {
        mOrderedProductList = orderedProductList;
    }
}
