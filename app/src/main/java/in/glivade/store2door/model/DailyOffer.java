package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Bobby on 01/08/17
 */

public class DailyOffer {

    @SerializedName("id")
    private int mId;
    @SerializedName("discount")
    private int mDiscount;
    @SerializedName("product")
    private Product mProduct;

    public DailyOffer(int id, int discount, Product product) {
        mId = id;
        mDiscount = discount;
        mProduct = product;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getDiscount() {
        return mDiscount;
    }

    public void setDiscount(int discount) {
        mDiscount = discount;
    }

    public Product getProduct() {
        return mProduct;
    }

    public void setProduct(Product product) {
        mProduct = product;
    }
}
