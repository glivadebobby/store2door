package in.glivade.store2door.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Bobby on 02/08/17
 */

public class NonFieldError {

    @SerializedName("non_field_errors")
    private List<String> mNonFieldErrorList;

    public NonFieldError(List<String> nonFieldErrorList) {
        mNonFieldErrorList = nonFieldErrorList;
    }

    public List<String> getNonFieldErrorList() {
        return mNonFieldErrorList;
    }

    public void setNonFieldErrorList(List<String> nonFieldErrorList) {
        mNonFieldErrorList = nonFieldErrorList;
    }

    public String getMessage() {
        return mNonFieldErrorList.isEmpty() ? null : mNonFieldErrorList.get(0);
    }
}
