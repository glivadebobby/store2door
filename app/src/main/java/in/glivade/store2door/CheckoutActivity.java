package in.glivade.store2door;

import static in.glivade.store2door.R.id.address;
import static in.glivade.store2door.app.Constant.EXTRA_AMOUNT;
import static in.glivade.store2door.app.Constant.EXTRA_LAUNCH;
import static in.glivade.store2door.app.MyActivity.launchClearStackWithBundle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.razorpay.PaymentData;
import com.razorpay.PaymentResultWithDataListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.glivade.store2door.adapter.AddressAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.AddAddressCallback;
import in.glivade.store2door.callback.AddressCallback;
import in.glivade.store2door.fragment.AddAddressDialogFragment;
import in.glivade.store2door.model.Address;
import in.glivade.store2door.model.Checkout;
import in.glivade.store2door.model.Data;
import in.glivade.store2door.model.PaymentSuccess;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CheckoutActivity extends AppCompatActivity implements AddressCallback,
        View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, Runnable, AddAddressCallback,
        PaymentResultWithDataListener {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private LinearLayout mLayoutAddress;
    private TextView mTextViewAddAddress, mTextViewTotal;
    private RelativeLayout mLayoutPayment;
    private LinearLayout mLayoutProgress;
    private RecyclerView mViewAddresses;
    private List<Address> mAddressList;
    private AddressAdapter mAddressAdapter;
    private MyPreference mPreference;
    private int mSelectedAddress;
    private boolean mProcessing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAddressClick(int position) {
        if (!mProcessing) {
            resetAddressSelection();
            Address address = mAddressList.get(position);
            mSelectedAddress = address.getId();
            address.setSelected(true);
            mAddressAdapter.notifyItemChanged(position);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mTextViewAddAddress && !mProcessing) {
            new AddAddressDialogFragment().show(getSupportFragmentManager(), "add-address");
        } else if (view == mLayoutPayment) {
            displayProgress(true);
            checkoutCart();
        }
    }

    @Override
    public void onAddressAdded(Address address) {
        resetAddressSelection();
        address.setSelected(true);
        mSelectedAddress = address.getId();
        mAddressList.add(address);
        mAddressAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        getAddresses();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getAddresses();
    }

    @Override
    public void onPaymentSuccess(String s, PaymentData paymentData) {
        paymentSuccess(new PaymentSuccess(1, mSelectedAddress, paymentData.getOrderId(),
                paymentData.getPaymentId(), paymentData.getSignature()));
    }

    @Override
    public void onPaymentError(int i, String s, PaymentData paymentData) {
        displayProgress(false);
        ToastBuilder.build(mContext, s);
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mLayoutAddress = (LinearLayout) findViewById(address);
        mTextViewAddAddress = (TextView) findViewById(R.id.txt_add_address);
        mTextViewTotal = (TextView) findViewById(R.id.txt_total);
        mViewAddresses = (RecyclerView) findViewById(R.id.addresses);
        mLayoutProgress = (LinearLayout) findViewById(R.id.progress);
        mLayoutPayment = (RelativeLayout) findViewById(R.id.payment);

        mContext = this;
        mAddressList = new ArrayList<>();
        mAddressAdapter = new AddressAdapter(mContext, mAddressList, this);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mTextViewAddAddress.setOnClickListener(this);
        mLayoutPayment.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        mViewAddresses.setLayoutManager(new LinearLayoutManager(mContext));
        mViewAddresses.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mViewAddresses.setAdapter(mAddressAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mTextViewTotal.setText(
                    String.format(Locale.getDefault(), getString(R.string.format_price),
                            bundle.getInt(EXTRA_AMOUNT)));
        }
    }

    private void getAddresses() {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<List<Address>> call = cartService.getAddresses("Token " + mPreference.getToken());
        call.enqueue(new Callback<List<Address>>() {
            @Override
            public void onResponse(@NonNull Call<List<Address>> call,
                    @NonNull Response<List<Address>> response) {
                List<Address> addressListResponse = response.body();
                if (response.isSuccessful() && addressListResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mLayoutAddress.setVisibility(View.VISIBLE);
                    mAddressList.clear();
                    mAddressList.addAll(addressListResponse);
                    mAddressAdapter.notifyDataSetChanged();
                    if (!mAddressList.isEmpty()) {
                        Address address = mAddressList.get(0);
                        address.setSelected(true);
                        mSelectedAddress = address.getId();
                        mAddressAdapter.notifyItemChanged(0);
                    }
                    com.razorpay.Checkout.preload(mContext);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Address>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void checkoutCart() {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<Checkout> call = cartService.checkoutCart("Token " + mPreference.getToken());
        call.enqueue(new Callback<Checkout>() {
            @Override
            public void onResponse(@NonNull Call<Checkout> call,
                    @NonNull Response<Checkout> response) {
                Checkout checkoutResponse = response.body();
                if (response.isSuccessful() && checkoutResponse != null) {
                    processPayment(checkoutResponse.getAmount(), checkoutResponse.getOrderId());
                }
            }

            @Override
            public void onFailure(@NonNull Call<Checkout> call, @NonNull Throwable t) {
                displayProgress(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void processPayment(int amount, String orderId) {
        com.razorpay.Checkout checkout = new com.razorpay.Checkout();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", getString(R.string.app_name));
            jsonObject.put("description", "Product Purchases");
            jsonObject.put("currency", "INR");
            jsonObject.put("amount", amount);
            JSONObject themeObject = new JSONObject();
            themeObject.put("color", "#e74c3c");
            themeObject.put("emi_mode", true);
            jsonObject.put("theme", themeObject);
            JSONObject prefillObject = new JSONObject();
            prefillObject.put("name", mPreference.getName());
            prefillObject.put("contact", mPreference.getPhone());
            prefillObject.put("email", mPreference.getEmail());
            jsonObject.put("prefill", prefillObject);
            jsonObject.put("order_id", orderId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        checkout.open(this, jsonObject);
    }

    private void paymentSuccess(PaymentSuccess paymentSuccess) {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<Data> call = cartService.paymentSuccess("Token " + mPreference.getToken(),
                paymentSuccess);
        call.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(@NonNull Call<Data> call, @NonNull Response<Data> response) {
                Data dataResponse = response.body();
                if (response.isSuccessful() && dataResponse != null) {
                    ToastBuilder.build(mContext, dataResponse.getData());
                    Bundle bundle = new Bundle();
                    bundle.putBoolean(EXTRA_LAUNCH, true);
                    launchClearStackWithBundle(mContext, OrderActivity.class, bundle);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Data> call, @NonNull Throwable t) {
                displayProgress(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void resetAddressSelection() {
        for (Address address : mAddressList) {
            address.setSelected(false);
        }
        mAddressAdapter.notifyDataSetChanged();
    }

    private void displayProgress(boolean show) {
        if (show) {
            mProcessing = true;
            mLayoutProgress.setVisibility(View.VISIBLE);
            mLayoutPayment.setVisibility(View.GONE);
        } else {
            mProcessing = false;
            mLayoutProgress.setVisibility(View.GONE);
            mLayoutPayment.setVisibility(View.VISIBLE);
        }
    }
}
