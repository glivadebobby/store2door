package in.glivade.store2door;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.glivade.store2door.adapter.AddressAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.AddAddressCallback;
import in.glivade.store2door.callback.AddressCallback;
import in.glivade.store2door.fragment.AddAddressDialogFragment;
import in.glivade.store2door.model.Address;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AddressActivity extends AppCompatActivity implements AddressCallback,
        View.OnClickListener, SwipeRefreshLayout.OnRefreshListener, Runnable, AddAddressCallback {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private LinearLayout mLayoutAddress;
    private TextView mTextViewAddAddress;
    private RecyclerView mViewAddresses;
    private List<Address> mAddressList;
    private AddressAdapter mAddressAdapter;
    private MyPreference mPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAddressClick(int position) {

    }

    @Override
    public void onClick(View view) {
        if (view == mTextViewAddAddress) {
            new AddAddressDialogFragment().show(getSupportFragmentManager(), "add-address");
        }
    }

    @Override
    public void onAddressAdded(Address address) {
        mAddressList.add(address);
        mAddressAdapter.notifyDataSetChanged();
    }

    @Override
    public void onRefresh() {
        getAddresses();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getAddresses();
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mLayoutAddress = (LinearLayout) findViewById(R.id.address);
        mTextViewAddAddress = (TextView) findViewById(R.id.txt_add_address);
        mViewAddresses = (RecyclerView) findViewById(R.id.addresses);

        mContext = this;
        mAddressList = new ArrayList<>();
        mAddressAdapter = new AddressAdapter(mContext, mAddressList, this);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
        mTextViewAddAddress.setOnClickListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        mViewAddresses.setLayoutManager(new LinearLayoutManager(mContext));
        mViewAddresses.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mViewAddresses.setAdapter(mAddressAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void getAddresses() {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<List<Address>> call = cartService.getAddresses("Token " + mPreference.getToken());
        call.enqueue(new Callback<List<Address>>() {
            @Override
            public void onResponse(@NonNull Call<List<Address>> call,
                    @NonNull Response<List<Address>> response) {
                List<Address> addressListResponse = response.body();
                if (response.isSuccessful() && addressListResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mLayoutAddress.setVisibility(View.VISIBLE);
                    mAddressList.clear();
                    mAddressList.addAll(addressListResponse);
                    mAddressAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Address>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }
}
