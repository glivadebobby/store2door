package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.PromotionCallback;


/**
 * Created by Bobby on 15/07/17
 */

public class PromotionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mImageViewPromotion;
    private PromotionCallback mCallback;

    public PromotionHolder(View itemView, PromotionCallback callback) {
        super(itemView);
        mImageViewPromotion = itemView.findViewById(R.id.img_promotion);
        mCallback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onPromotionClick(getAdapterPosition());
        }
    }
}
