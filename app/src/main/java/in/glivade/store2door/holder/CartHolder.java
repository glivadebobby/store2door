package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.CartCallback;

/**
 * Created by Bobby on 02/08/17
 */

public class CartHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mImageViewProduct, mImageViewMinus, mImageViewPlus;
    public TextView mTextViewName, mTextViewUnit, mTextViewActualPrice, mTextViewQuantity;
    private CartCallback mCallback;

    public CartHolder(View itemView, CartCallback callback) {
        super(itemView);
        mImageViewProduct = itemView.findViewById(R.id.img_product);
        mImageViewMinus = itemView.findViewById(R.id.img_minus);
        mImageViewPlus = itemView.findViewById(R.id.img_plus);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mTextViewUnit = itemView.findViewById(R.id.txt_unit);
        mTextViewActualPrice = itemView.findViewById(R.id.txt_actual_price);
        mTextViewQuantity = itemView.findViewById(R.id.txt_quantity);
        mCallback = callback;
        mImageViewProduct.setOnClickListener(this);
        mImageViewMinus.setOnClickListener(this);
        mImageViewPlus.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == mImageViewProduct) {
            mCallback.onProductClick(getAdapterPosition());
        } else if (view == mImageViewMinus) {
            mCallback.onMinusClick(getAdapterPosition());
        } else if (view == mImageViewPlus) {
            mCallback.onPlusClick(getAdapterPosition());
        }
    }
}
