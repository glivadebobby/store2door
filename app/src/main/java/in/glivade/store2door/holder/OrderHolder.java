package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.glivade.store2door.R;

/**
 * Created by Bobby on 04/08/17
 */

public class OrderHolder extends RecyclerView.ViewHolder {

    public TextView mTextViewOrderId, mTextViewDate, mTextViewAddress, mTextViewSeller,
            mTextViewTotal;
    public RecyclerView mViewProducts;

    public OrderHolder(View itemView) {
        super(itemView);
        mTextViewOrderId = itemView.findViewById(R.id.txt_order_id);
        mTextViewDate = itemView.findViewById(R.id.txt_date);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mTextViewSeller = itemView.findViewById(R.id.txt_seller);
        mTextViewTotal = itemView.findViewById(R.id.txt_total);
        mViewProducts = itemView.findViewById(R.id.products);
    }
}
