package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.store2door.R;

/**
 * Created by Bobby on 04/08/17
 */

public class OrderedProductHolder extends RecyclerView.ViewHolder {

    public ImageView mImageViewProduct;
    public TextView mTextViewName, mTextViewUnit, mTextViewQuantityPrice;

    public OrderedProductHolder(View itemView) {
        super(itemView);
        mImageViewProduct = itemView.findViewById(R.id.img_product);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mTextViewUnit = itemView.findViewById(R.id.txt_unit);
        mTextViewQuantityPrice = itemView.findViewById(R.id.txt_quantity_price);
    }
}
