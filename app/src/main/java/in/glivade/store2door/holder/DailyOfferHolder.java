package in.glivade.store2door.holder;

import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.ProductCallback;

/**
 * Created by Bobby on 16/07/17
 */

public class DailyOfferHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mImageViewProduct;
    public TextView mTextViewDiscount, mTextViewName, mTextViewUnit, mTextViewApparentPrice,
            mTextViewActualPrice, mTextViewAdd;
    private ProductCallback mCallback;

    public DailyOfferHolder(View itemView, ProductCallback callback) {
        super(itemView);
        mImageViewProduct = itemView.findViewById(R.id.img_product);
        mTextViewDiscount = itemView.findViewById(R.id.txt_discount);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mTextViewUnit = itemView.findViewById(R.id.txt_unit);
        mTextViewApparentPrice = itemView.findViewById(R.id.txt_apparent_price);
        mTextViewActualPrice = itemView.findViewById(R.id.txt_actual_price);
        mTextViewAdd = itemView.findViewById(R.id.txt_add);
        mTextViewApparentPrice.setPaintFlags(
                mTextViewApparentPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        mCallback = callback;
        itemView.setOnClickListener(this);
        mTextViewAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onProductClick(getAdapterPosition());
        } else if (view == mTextViewAdd) {
            mCallback.onAddClick(getAdapterPosition());
        }
    }
}
