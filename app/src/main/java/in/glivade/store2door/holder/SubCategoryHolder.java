package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.SubCategoryCallback;

/**
 * Created by Bobby on 16/07/17
 */

public class SubCategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mImageViewSubCategory;
    public TextView mTextViewName;
    private SubCategoryCallback mCallback;

    public SubCategoryHolder(View itemView, SubCategoryCallback callback) {
        super(itemView);
        mImageViewSubCategory = itemView.findViewById(R.id.img_sub_category);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mCallback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onSubCategoryClick(getAdapterPosition());
        }
    }
}
