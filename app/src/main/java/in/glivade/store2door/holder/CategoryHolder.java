package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.CategoryCallback;

/**
 * Created by Bobby on 16/07/17
 */

public class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mImageViewCategory;
    public TextView mTextViewName, mTextViewDesc;
    public ImageView mImageViewExpand;
    public RecyclerView mViewSubCategories;
    private CategoryCallback mCallback;

    public CategoryHolder(View itemView, CategoryCallback callback) {
        super(itemView);
        mImageViewCategory = itemView.findViewById(R.id.img_category);
        mTextViewName = itemView.findViewById(R.id.txt_name);
        mTextViewDesc = itemView.findViewById(R.id.txt_unit);
        mImageViewExpand = itemView.findViewById(R.id.img_expand);
        mViewSubCategories = itemView.findViewById(R.id.sub_categories);
        mCallback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onCategoryClick(getAdapterPosition());
        } else if (view == mImageViewExpand) {
            mCallback.onCategoryClick(getAdapterPosition());
        }
    }
}
