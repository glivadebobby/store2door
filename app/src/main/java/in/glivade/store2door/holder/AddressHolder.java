package in.glivade.store2door.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.AddressCallback;

/**
 * Created by Bobby on 02/08/17
 */

public class AddressHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public View mViewSelected;
    public TextView mTextViewAddress;
    private AddressCallback mCallback;

    public AddressHolder(View itemView, AddressCallback callback) {
        super(itemView);
        mViewSelected = itemView.findViewById(R.id.selected);
        mTextViewAddress = itemView.findViewById(R.id.txt_address);
        mCallback = callback;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == itemView) {
            mCallback.onAddressClick(getAdapterPosition());
        }
    }
}
