package in.glivade.store2door;

import static in.glivade.store2door.app.Constant.EXTRA_ID;
import static in.glivade.store2door.app.Constant.EXTRA_TITLE;
import static in.glivade.store2door.app.MyActivity.launchWithBundle;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

import in.glivade.store2door.adapter.ProductAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.api.ProductService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.ProductCallback;
import in.glivade.store2door.model.CreateCart;
import in.glivade.store2door.model.NonFieldError;
import in.glivade.store2door.model.Product;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Converter;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProductActivity extends AppCompatActivity implements ProductCallback,
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mViewProducts;
    private List<Product> mProductList;
    private ProductAdapter mProductAdapter;
    private MyPreference mPreference;
    private int mId;
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        initObjects();
        initCallbacks();
        processBundle();
        initToolbar();
        initRecyclerView();
        initRefresh();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mPreference != null) getProducts();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onProductClick(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_ID, mProductList.get(position).getId());
        launchWithBundle(mContext, ProductDetailActivity.class, bundle);
    }

    @Override
    public void onAddClick(int position) {
        Product product = mProductList.get(position);
        if (!product.isInCart()) addToCart(new CreateCart(product.getId(), 1));
    }

    @Override
    public void onRefresh() {
        getProducts();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getProducts();
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mViewProducts = (RecyclerView) findViewById(R.id.products);

        mContext = this;
        mProductList = new ArrayList<>();
        mProductAdapter = new ProductAdapter(mContext, mProductList, this);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mId = bundle.getInt(EXTRA_ID);
            mTitle = bundle.getString(EXTRA_TITLE, null);
        }
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(mTitle);
        }
    }

    private void initRecyclerView() {
        mViewProducts.setLayoutManager(new LinearLayoutManager(mContext));
        mViewProducts.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        mViewProducts.setAdapter(mProductAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void getProducts() {
        ProductService productService = ApiClient.getClient().create(ProductService.class);
        Call<List<Product>> call = productService.getProducts("Token " + mPreference.getToken(),
                mId);
        call.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(@NonNull Call<List<Product>> call,
                    @NonNull Response<List<Product>> response) {
                List<Product> productListResponse = response.body();
                if (response.isSuccessful() && productListResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mProductList.clear();
                    mProductList.addAll(productListResponse);
                    mProductAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Product>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void addToCart(CreateCart createCart) {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<CreateCart> call = cartService.createCart("Token " + mPreference.getToken(),
                createCart);
        call.enqueue(new Callback<CreateCart>() {
            @Override
            public void onResponse(@NonNull Call<CreateCart> call,
                    @NonNull Response<CreateCart> response) {
                CreateCart createCartResponse = response.body();
                if (response.isSuccessful() && createCartResponse != null) {
                    for (int i = 0; i < mProductList.size(); i++) {
                        Product product = mProductList.get(i);
                        if (createCartResponse.getProduct() == product.getId()) {
                            product.setInCart(true);
                            mProductAdapter.notifyItemChanged(i);
                            break;
                        }
                    }
                } else {
                    processError(response);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CreateCart> call, @NonNull Throwable t) {
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }

    private void processError(Response<CreateCart> response) {
        int statusCode = response.code();
        if (statusCode == 400 && response.errorBody() != null) {
            try {
                Converter<ResponseBody, NonFieldError> converter =
                        ApiClient.getClient().responseBodyConverter(NonFieldError.class,
                                new Annotation[0]);
                //noinspection ConstantConditions
                NonFieldError nonFieldError = converter.convert(response.errorBody());
                if (nonFieldError != null) {
                    ToastBuilder.build(mContext, nonFieldError.getMessage());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            ToastBuilder.build(mContext, getString(R.string.error_unexpected));
        }
    }
}
