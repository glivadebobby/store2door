package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.glivade.store2door.R;
import in.glivade.store2door.holder.OrderedProductHolder;
import in.glivade.store2door.model.OrderedProduct;

/**
 * Created by Bobby on 04/08/17
 */

public class OrderedProductAdapter extends RecyclerView.Adapter<OrderedProductHolder> {

    private Context mContext;
    private List<OrderedProduct> mOrderedProductList;

    public OrderedProductAdapter(Context context, List<OrderedProduct> orderedProductList) {
        mContext = context;
        mOrderedProductList = orderedProductList;
    }

    @Override
    public OrderedProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderedProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ordered_product,
                        parent, false));
    }

    @Override
    public void onBindViewHolder(OrderedProductHolder holder, int position) {
        OrderedProduct orderedProduct = mOrderedProductList.get(position);
        Glide.with(mContext)
                .load(orderedProduct.getProduct().getImage())
                .into(holder.mImageViewProduct);
        holder.mTextViewName.setText(orderedProduct.getProduct().getName());
        holder.mTextViewUnit.setText(orderedProduct.getProduct().getUnit());
        holder.mTextViewQuantityPrice.setText(String.format(Locale.getDefault(),
                mContext.getString(R.string.format_quantity_price),
                orderedProduct.getProduct().getActualPrice(), orderedProduct.getQuantity(),
                orderedProduct.getProduct().getActualPrice() * orderedProduct.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return mOrderedProductList.size();
    }
}
