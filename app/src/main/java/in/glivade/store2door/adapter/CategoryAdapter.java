package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.CategoryCallback;
import in.glivade.store2door.callback.SubCategoryCallback;
import in.glivade.store2door.holder.CategoryHolder;
import in.glivade.store2door.model.Category;

/**
 * Created by Bobby on 16/07/17
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryHolder> {

    private Context mContext;
    private List<Category> mCategoryList;
    private CategoryCallback mCategoryCallback;
    private SubCategoryCallback mSubCategoryCallback;

    public CategoryAdapter(Context context, List<Category> categoryList,
            CategoryCallback categoryCallback, SubCategoryCallback subCategoryCallback) {
        mContext = context;
        mCategoryList = categoryList;
        mCategoryCallback = categoryCallback;
        mSubCategoryCallback = subCategoryCallback;
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CategoryHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, parent,
                        false), mCategoryCallback);
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        Category category = mCategoryList.get(position);
        Glide.with(mContext)
                .load(category.getImage())
                .into(holder.mImageViewCategory);
        holder.mTextViewName.setText(category.getName());
        holder.mTextViewDesc.setText(category.getDescription());
        if (category.isSelected()) {
            holder.itemView.setBackgroundResource(R.color.green_30);
            holder.mImageViewExpand.setImageResource(R.drawable.ic_collapse);
            holder.mViewSubCategories.setVisibility(View.VISIBLE);
            holder.mViewSubCategories.setLayoutManager(new GridLayoutManager(mContext, 3));
            holder.mViewSubCategories.setAdapter(
                    new SubCategoryAdapter(mContext, category.getSubCategoryList(),
                            mSubCategoryCallback));
        } else {
            holder.itemView.setBackgroundResource(android.R.color.white);
            holder.mImageViewExpand.setImageResource(R.drawable.ic_expand);
            holder.mViewSubCategories.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }
}
