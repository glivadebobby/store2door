package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.CartCallback;
import in.glivade.store2door.holder.CartHolder;
import in.glivade.store2door.model.Cart;

/**
 * Created by Bobby on 02/08/17
 */

public class CartAdapter extends RecyclerView.Adapter<CartHolder> {

    private Context mContext;
    private List<Cart> mCartList;
    private CartCallback mCallback;

    public CartAdapter(Context context, List<Cart> cartList, CartCallback callback) {
        mContext = context;
        mCartList = cartList;
        mCallback = callback;
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CartHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false),
                mCallback);
    }

    @Override
    public void onBindViewHolder(CartHolder holder, int position) {
        Cart cart = mCartList.get(position);
        Glide.with(mContext)
                .load(cart.getProduct().getImage())
                .into(holder.mImageViewProduct);
        holder.mTextViewName.setText(cart.getProduct().getName());
        holder.mTextViewUnit.setText(cart.getProduct().getUnit());
        holder.mTextViewQuantity.setText(String.valueOf(cart.getQuantity()));
        holder.mTextViewActualPrice.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_price),
                        cart.getProduct().getActualPrice() * cart.getQuantity()));
    }

    @Override
    public int getItemCount() {
        return mCartList.size();
    }
}
