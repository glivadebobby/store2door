package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.ProductCallback;
import in.glivade.store2door.holder.ProductHolder;
import in.glivade.store2door.model.Product;

/**
 * Created by Bobby on 16/07/17
 */

public class ProductAdapter extends RecyclerView.Adapter<ProductHolder> {

    private Context mContext;
    private List<Product> mProductList;
    private ProductCallback mCallback;

    public ProductAdapter(Context context, List<Product> productList, ProductCallback callback) {
        mContext = context;
        mProductList = productList;
        mCallback = callback;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProductHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(ProductHolder holder, int position) {
        Product product = mProductList.get(position);
        Glide.with(mContext)
                .load(product.getImage())
                .into(holder.mImageViewProduct);
        if (product.getDiscount().equals("0.0")) {
            holder.mTextViewDiscount.setVisibility(View.GONE);
        } else {
            holder.mTextViewDiscount.setVisibility(View.VISIBLE);
            holder.mTextViewDiscount.setText(product.getDiscount());
        }
        holder.mTextViewName.setText(product.getName());
        holder.mTextViewUnit.setText(product.getUnit());
        holder.mTextViewApparentPrice.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_price),
                        product.getApparentPrice()));
        holder.mTextViewActualPrice.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_price),
                        product.getActualPrice()));
        if (product.isInCart()) {
            holder.mTextViewAdd.setBackground(null);
            holder.mTextViewAdd.setText(mContext.getString(R.string.txt_added_cart));
        } else {
            holder.mTextViewAdd.setBackgroundResource(R.drawable.bg_primary_stroke_round);
            holder.mTextViewAdd.setText(mContext.getString(R.string.txt_add));
        }
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }
}
