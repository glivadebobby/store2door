package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.ProductCallback;
import in.glivade.store2door.holder.DailyOfferHolder;
import in.glivade.store2door.model.DailyOffer;
import in.glivade.store2door.model.Product;

/**
 * Created by Bobby on 16/07/17
 */

public class DailyOfferAdapter extends RecyclerView.Adapter<DailyOfferHolder> {

    private Context mContext;
    private List<DailyOffer> mDailyOfferList;
    private ProductCallback mCallback;

    public DailyOfferAdapter(Context context, List<DailyOffer> dailyOfferList,
            ProductCallback callback) {
        mContext = context;
        mDailyOfferList = dailyOfferList;
        mCallback = callback;
    }

    @Override
    public DailyOfferHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DailyOfferHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_daily_offer, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(DailyOfferHolder holder, int position) {
        DailyOffer dailyOffer = mDailyOfferList.get(position);
        Product product = dailyOffer.getProduct();
        Glide.with(mContext)
                .load(product.getImage())
                .into(holder.mImageViewProduct);
        holder.mTextViewDiscount.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_discount),
                        dailyOffer.getDiscount()));
        holder.mTextViewName.setText(product.getName());
        holder.mTextViewUnit.setText(product.getUnit());
        holder.mTextViewApparentPrice.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_price),
                        product.getApparentPrice()));
        holder.mTextViewActualPrice.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_price),
                        product.getActualPrice()));
        if (dailyOffer.getProduct().isInCart()) {
            holder.mTextViewAdd.setBackground(null);
            holder.mTextViewAdd.setText(mContext.getString(R.string.txt_added_cart));
        } else {
            holder.mTextViewAdd.setBackgroundResource(R.drawable.bg_primary_stroke_round);
            holder.mTextViewAdd.setText(mContext.getString(R.string.txt_add));
        }
    }

    @Override
    public int getItemCount() {
        return mDailyOfferList.size();
    }
}
