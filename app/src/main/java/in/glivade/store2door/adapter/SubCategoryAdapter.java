package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.SubCategoryCallback;
import in.glivade.store2door.holder.SubCategoryHolder;
import in.glivade.store2door.model.SubCategory;

/**
 * Created by Bobby on 16/07/17
 */

public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryHolder> {

    private Context mContext;
    private List<SubCategory> mSubCategoryList;
    private SubCategoryCallback mCallback;

    public SubCategoryAdapter(Context context, List<SubCategory> subCategoryList,
            SubCategoryCallback callback) {
        mContext = context;
        mSubCategoryList = subCategoryList;
        mCallback = callback;
    }

    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SubCategoryHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_category, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(SubCategoryHolder holder, int position) {
        SubCategory subCategory = mSubCategoryList.get(position);
        Glide.with(mContext)
                .load(subCategory.getImage())
                .into(holder.mImageViewSubCategory);
        holder.mTextViewName.setText(subCategory.getName());
    }

    @Override
    public int getItemCount() {
        return mSubCategoryList.size();
    }
}
