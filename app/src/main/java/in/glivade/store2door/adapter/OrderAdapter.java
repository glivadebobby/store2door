package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;
import java.util.Locale;

import in.glivade.store2door.R;
import in.glivade.store2door.holder.OrderHolder;
import in.glivade.store2door.model.Order;

/**
 * Created by Bobby on 04/08/17
 */

public class OrderAdapter extends RecyclerView.Adapter<OrderHolder> {

    private Context mContext;
    private List<Order> mOrderList;

    public OrderAdapter(Context context, List<Order> orderList) {
        mContext = context;
        mOrderList = orderList;
    }

    @Override
    public OrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order, parent,
                        false));
    }

    @Override
    public void onBindViewHolder(OrderHolder holder, int position) {
        Order order = mOrderList.get(position);
        holder.mTextViewOrderId.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_order_id),
                        order.getId()));
        holder.mTextViewDate.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_ordered_on),
                        order.getCreatedOn()));
        holder.mTextViewAddress.setText(order.getAddress().getAddress());
        holder.mTextViewSeller.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_seller),
                        order.getStore().getName(), order.getStore().getRating()));
        holder.mTextViewTotal.setText(
                String.format(Locale.getDefault(), mContext.getString(R.string.format_total),
                        order.getTotalCost()));
        holder.mViewProducts.setLayoutManager(new LinearLayoutManager(mContext));
        holder.mViewProducts.addItemDecoration(
                new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));
        holder.mViewProducts.setAdapter(
                new OrderedProductAdapter(mContext, order.getOrderedProductList()));
    }

    @Override
    public int getItemCount() {
        return mOrderList.size();
    }
}
