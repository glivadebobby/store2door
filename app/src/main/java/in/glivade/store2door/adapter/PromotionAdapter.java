package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;

import java.util.List;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.PromotionCallback;
import in.glivade.store2door.holder.PromotionHolder;
import in.glivade.store2door.model.Promotion;


/**
 * Created by Bobby on 15/07/17
 */

public class PromotionAdapter extends RecyclerView.Adapter<PromotionHolder> {

    private Context mContext;
    private List<Promotion> mPromotionList;
    private PromotionCallback mCallback;

    public PromotionAdapter(Context context, List<Promotion> promotionList,
            PromotionCallback callback) {
        mContext = context;
        mPromotionList = promotionList;
        mCallback = callback;
    }

    @Override
    public PromotionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new PromotionHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_promotion, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(PromotionHolder holder, int position) {
        Promotion promotion = mPromotionList.get(position);
        Glide.with(mContext).load(promotion.getImage()).into(holder.mImageViewPromotion);
    }

    @Override
    public int getItemCount() {
        return mPromotionList.size();
    }
}
