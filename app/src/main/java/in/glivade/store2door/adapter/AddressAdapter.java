package in.glivade.store2door.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import in.glivade.store2door.R;
import in.glivade.store2door.callback.AddressCallback;
import in.glivade.store2door.holder.AddressHolder;
import in.glivade.store2door.model.Address;

/**
 * Created by Bobby on 02/08/17
 */

public class AddressAdapter extends RecyclerView.Adapter<AddressHolder> {

    private Context mContext;
    private List<Address> mAddressList;
    private AddressCallback mCallback;

    public AddressAdapter(Context context, List<Address> addressList, AddressCallback callback) {
        mContext = context;
        mAddressList = addressList;
        mCallback = callback;
    }

    @Override
    public AddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new AddressHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_address, parent,
                        false), mCallback);
    }

    @Override
    public void onBindViewHolder(AddressHolder holder, int position) {
        Address address = mAddressList.get(position);
        holder.mViewSelected.setVisibility(address.isSelected() ? View.VISIBLE : View.INVISIBLE);
        holder.mTextViewAddress.setText(address.getAddress());
    }

    @Override
    public int getItemCount() {
        return mAddressList.size();
    }
}
