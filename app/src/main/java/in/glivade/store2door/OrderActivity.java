package in.glivade.store2door;

import static in.glivade.store2door.app.Constant.EXTRA_LAUNCH;
import static in.glivade.store2door.app.MyActivity.launchClearStack;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import in.glivade.store2door.adapter.OrderAdapter;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.model.Order;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, Runnable {

    private Context mContext;
    private Toolbar mToolbar;
    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mViewOrders;
    private List<Order> mOrderList;
    private OrderAdapter mOrderAdapter;
    private MyPreference mPreference;
    private boolean mLaunchMainActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        initObjects();
        initCallbacks();
        initToolbar();
        initRecyclerView();
        initRefresh();
        processBundle();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mLaunchMainActivity) {
            launchClearStack(mContext, MainActivity.class);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRefresh() {
        getOrders();
    }

    @Override
    public void run() {
        mRefreshLayout.setRefreshing(true);
        getOrders();
    }

    private void initObjects() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        mViewOrders = (RecyclerView) findViewById(R.id.orders);

        mContext = this;
        mOrderList = new ArrayList<>();
        mOrderAdapter = new OrderAdapter(mContext, mOrderList);
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mRefreshLayout.setOnRefreshListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initRecyclerView() {
        mViewOrders.setLayoutManager(new LinearLayoutManager(mContext));
        mViewOrders.setAdapter(mOrderAdapter);
    }

    private void initRefresh() {
        mRefreshLayout.setColorSchemeResources(R.color.primary, R.color.primary_dark,
                R.color.accent);
        mRefreshLayout.post(this);
    }

    private void processBundle() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mLaunchMainActivity = bundle.getBoolean(EXTRA_LAUNCH);
        }
    }

    private void getOrders() {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<List<Order>> call = cartService.getOrders("Token " + mPreference.getToken());
        call.enqueue(new Callback<List<Order>>() {
            @Override
            public void onResponse(@NonNull Call<List<Order>> call,
                    @NonNull Response<List<Order>> response) {
                List<Order> orderListResponse = response.body();
                if (response.isSuccessful() && orderListResponse != null) {
                    mRefreshLayout.setRefreshing(false);
                    mOrderList.clear();
                    mOrderList.addAll(orderListResponse);
                    mOrderAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Order>> call, @NonNull Throwable t) {
                mRefreshLayout.setRefreshing(false);
                ToastBuilder.build(mContext, t.getMessage());
            }
        });
    }
}
