package in.glivade.store2door.app;

import android.app.Application;

import in.glivade.store2door.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Bobby on 14-07-2017
 */

public class AppController extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Rubik-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
