package in.glivade.store2door.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;

import com.facebook.accountkit.AccountKit;

/**
 * Created by Bobby on 31/07/17
 */

public class MyPreference {

    private static final String PREF_USER = "user";
    private static final String PREF_EXTRAS = "extras";
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String EMAIL = "email";
    private static final String TOKEN = "token";
    private static final String CART_COUNT = "cart_count";
    private static final String FCM_TOKEN = "fcm_token";
    private static final String TOKEN_UPLOADED = "token_uploaded";
    private static final String FIRST_TIME = "first_time";
    private SharedPreferences mPreferencesUser, mPreferencesExtras;

    public MyPreference(Context context) {
        mPreferencesUser = context.getSharedPreferences(PREF_USER, Context.MODE_PRIVATE);
        mPreferencesExtras = context.getSharedPreferences(PREF_EXTRAS, Context.MODE_PRIVATE);
    }

    public String getName() {
        return mPreferencesUser.getString(encode(NAME), null);
    }

    public void setName(String name) {
        mPreferencesUser.edit().putString(encode(NAME), name).apply();
    }

    public String getPhone() {
        return mPreferencesUser.getString(encode(PHONE), null);
    }

    public void setPhone(String phone) {
        mPreferencesUser.edit().putString(encode(PHONE), phone).apply();
    }

    public String getEmail() {
        return mPreferencesUser.getString(encode(EMAIL), null);
    }

    public void setEmail(String email) {
        mPreferencesUser.edit().putString(encode(EMAIL), email).apply();
    }

    public String getToken() {
        return mPreferencesUser.getString(encode(TOKEN), null);
    }

    public void setToken(String token) {
        mPreferencesUser.edit().putString(encode(TOKEN), token).apply();
    }

    public int getCartCount() {
        return mPreferencesUser.getInt(encode(CART_COUNT), 0);
    }

    public void setCartCount(int cartCount) {
        mPreferencesUser.edit().putInt(encode(CART_COUNT), cartCount).apply();
    }

    public void setTokenUploaded(boolean tokenUploaded) {
        mPreferencesExtras.edit().putBoolean(encode(TOKEN_UPLOADED), tokenUploaded).apply();
    }

    public String getFcmToken() {
        return mPreferencesExtras.getString(encode(FCM_TOKEN), null);
    }

    void setFcmToken(String fcmToken) {
        mPreferencesExtras.edit().putString(encode(FCM_TOKEN), fcmToken).apply();
    }

    public boolean isFirstTime() {
        return mPreferencesExtras.getBoolean(encode(FIRST_TIME), true);
    }

    public void setFirstTime(boolean firstTime) {
        mPreferencesExtras.edit().putBoolean(encode(FIRST_TIME), firstTime).apply();
    }

    public void clearUser() {
        AccountKit.logOut();
        setTokenUploaded(false);
        mPreferencesUser.edit().clear().apply();
    }

    private String encode(String data) {
        return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP);
    }
}
