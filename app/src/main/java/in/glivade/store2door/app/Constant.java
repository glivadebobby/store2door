package in.glivade.store2door.app;

/**
 * Created by Bobby on 01/08/17
 */

public class Constant {
    /**
     * Bundle extras
     */
    public static final String EXTRA_ID = "id";
    public static final String EXTRA_TITLE = "title";
    public static final String EXTRA_AMOUNT = "amount";
    public static final String EXTRA_LAUNCH = "launch";
}
