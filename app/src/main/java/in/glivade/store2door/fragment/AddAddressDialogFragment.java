package in.glivade.store2door.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.Locale;

import in.glivade.store2door.AddressActivity;
import in.glivade.store2door.R;
import in.glivade.store2door.api.ApiClient;
import in.glivade.store2door.api.CartService;
import in.glivade.store2door.app.MyPreference;
import in.glivade.store2door.app.ToastBuilder;
import in.glivade.store2door.callback.AddAddressCallback;
import in.glivade.store2door.model.Address;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bobby on 02/08/17
 */

public class AddAddressDialogFragment extends DialogFragment implements View.OnClickListener {

    private Context mContext;
    private View mRootView;
    private TextInputLayout mLayoutStreet, mLayoutArea, mLayoutCity, mLayoutState, mLayoutCountry,
            mLayoutZipCode;
    private TextInputEditText mEditTextStreet, mEditTextArea, mEditTextCity, mEditTextState,
            mEditTextCountry, mEditTextZipCode;
    private LinearLayout mLayoutProgress;
    private Button mButtonSubmit;
    private MyPreference mPreference;
    private AddAddressCallback mCallback;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.dialog_add_address, container, false);
        initObjects();
        initCallbacks();
        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof AddressActivity) {
            mCallback = (AddAddressCallback) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement AddAddressCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mCallback = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        Window window = getDialog().getWindow();
        if (window != null) {
            window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == mButtonSubmit) {
            disableErrors();
            processAddAddress();
        }
    }

    private void initObjects() {
        mLayoutStreet = mRootView.findViewById(R.id.street);
        mLayoutArea = mRootView.findViewById(R.id.area);
        mLayoutCity = mRootView.findViewById(R.id.city);
        mLayoutState = mRootView.findViewById(R.id.state);
        mLayoutCountry = mRootView.findViewById(R.id.country);
        mLayoutZipCode = mRootView.findViewById(R.id.zip_code);
        mEditTextStreet = mRootView.findViewById(R.id.input_street);
        mEditTextArea = mRootView.findViewById(R.id.input_area);
        mEditTextCity = mRootView.findViewById(R.id.input_city);
        mEditTextState = mRootView.findViewById(R.id.input_state);
        mEditTextCountry = mRootView.findViewById(R.id.input_country);
        mEditTextZipCode = mRootView.findViewById(R.id.input_zip_code);
        mLayoutProgress = mRootView.findViewById(R.id.progress);
        mButtonSubmit = mRootView.findViewById(R.id.btn_submit);

        mContext = getActivity();
        mPreference = new MyPreference(mContext);
    }

    private void initCallbacks() {
        mButtonSubmit.setOnClickListener(this);
    }

    private void disableErrors() {
        mLayoutStreet.setErrorEnabled(false);
        mLayoutArea.setErrorEnabled(false);
        mLayoutCity.setErrorEnabled(false);
        mLayoutState.setErrorEnabled(false);
        mLayoutCountry.setErrorEnabled(false);
        mLayoutZipCode.setErrorEnabled(false);
    }

    private void processAddAddress() {
        String street = mEditTextStreet.getText().toString().trim();
        String area = mEditTextArea.getText().toString().trim();
        String city = mEditTextCity.getText().toString().trim();
        String state = mEditTextState.getText().toString().trim();
        String country = mEditTextCountry.getText().toString().trim();
        String zipCode = mEditTextZipCode.getText().toString().trim();
        if (validateInput(street, area, city, state, country, zipCode)) {
            displayProgress(true);
            addAddress(new Address(street, area, city, state, country, zipCode));
        }
    }

    private boolean validateInput(String street, String area, String city, String state,
            String country, String zipCode) {
        if (street.isEmpty()) {
            mLayoutStreet.setError(getString(R.string.error_empty));
            mEditTextStreet.requestFocus();
            return false;
        } else if (area.isEmpty()) {
            mLayoutArea.setError(getString(R.string.error_empty));
            mEditTextArea.requestFocus();
            return false;
        } else if ((city.isEmpty())) {
            mLayoutCity.setError(getString(R.string.error_empty));
            mEditTextCity.requestFocus();
            return false;
        } else if (state.isEmpty()) {
            mLayoutState.setError(getString(R.string.error_empty));
            mEditTextState.requestFocus();
            return false;
        } else if (country.isEmpty()) {
            mLayoutCountry.setError(getString(R.string.error_empty));
            mEditTextCountry.requestFocus();
            return false;
        } else if (zipCode.isEmpty()) {
            mLayoutZipCode.setError(getString(R.string.error_empty));
            mEditTextZipCode.requestFocus();
            return false;
        } else if (zipCode.length() < 6) {
            mLayoutArea.setError(
                    String.format(Locale.getDefault(), getString(R.string.error_length),
                            "ZipCode", 6, "digits"));
            mEditTextArea.requestFocus();
            return false;
        }
        return true;
    }

    private void addAddress(Address address) {
        CartService cartService = ApiClient.getClient().create(CartService.class);
        Call<Address> call = cartService.addAddress("Token " + mPreference.getToken(), address);
        call.enqueue(new Callback<Address>() {
            @Override
            public void onResponse(@NonNull Call<Address> call,
                    @NonNull Response<Address> response) {
                if (isAdded()) {
                    Address addressResponse = response.body();
                    if (response.isSuccessful()) {
                        if (mCallback != null) {
                            mCallback.onAddressAdded(addressResponse);
                        }
                        dismiss();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Address> call, @NonNull Throwable t) {
                if (isAdded()) {
                    displayProgress(false);
                    ToastBuilder.build(mContext, t.getMessage());
                }
            }
        });
    }

    private void displayProgress(boolean show) {
        if (show) {
            mLayoutProgress.setVisibility(View.VISIBLE);
            mButtonSubmit.setVisibility(View.GONE);
        } else {
            mLayoutProgress.setVisibility(View.GONE);
            mButtonSubmit.setVisibility(View.VISIBLE);
        }
    }
}
